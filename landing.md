<span class="intro-text">
Jede Bürgergeneration stellt neue Anforderungen an ‚ihre‘ Stadt, die deshalb zum Ort ständiger Veränderungen wird. Paderborn ist über 1200 Jahre alt und hat sich oft gewandelt. Vieles von dem, das die Stadt viele Jahre prägte, wurde in dieser langen Zeit um- und neugebaut.

Den tiefsten Einschnitt erlebte und überstand die Paderstadt im 20. Jahrhundert. Das ‚alte‘ Paderborn verschwand beinahe endgültig in den großen Bombenangriffen vom 17. Januar und 27. März 1945. Vor allem der historische Innenstadtbereich wurde zerstört oder blieb nur rudimentär erhalten. Zwar wurde vieles wieder aufgebaut, mehr jedoch nach aktuellen Bedürfnissen neu errichtet.

Auf unserem virtuellen Rundgang durch die Innenstadt lebt die alte Stadt am Beispiel stadthistorisch bedeutender Plätze und Gebäude wieder auf. Ihnen stellen wir das heutige Paderborn gegenüber. Dies ermöglicht interessante Vergleiche zwischen Vergangenheit und Gegenwart.

Unternehmen Sie mit Hilfe unseres Innenstadtplans einen Rundgang durch Paderborn. Führen Sie den Mauszeiger auf einen der blauen Punkte im Plan, dann sehen Sie, um welchen Standort es sich handelt.
</span>